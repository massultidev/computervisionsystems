#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import os
import cv2
import glob
import numpy
import datetime

from utility.Resource import Resource

class ChessAR(object):
    
    # Camera calibration file name:
    _CAM_CALIB_FILE = "camera-calibration.npz"
    # Camera calibration images directory:
    _CAM_CALIB_IMAGES_DIR = "../calibration"
    # Termination conditions for iterative process of corner refinement:
    _CRITERIA = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    def __init__(self, boardColumns=3, boardRows=3):
        self._cbCols = boardColumns+1
        self._cbRows = boardRows+1
    ##
    
    def loadCalibration(self, path=None):
        if path is None:
            path = Resource.getResourcePath(self._CAM_CALIB_FILE)
        
        # Load camera 3D calibration details:
        with numpy.load(path) as calibrationFile:
            self._mtx = calibrationFile["mtx"]
            self._dist = calibrationFile["dist"]
            #self._rvecs = calibrationFile["rvecs"]
            #self._tvecs= calibrationFile["tvecs"]
    ##
    
    def calibrateCollect(self, frame):
        result = self._findGridCorners(frame)[0]
     
        if result == True:
            filename = datetime.datetime.now().strftime("%Y%m%d_%Hh%Mm%Ss%f") + ".jpg"
            cv2.imwrite(os.path.join(Resource.getResourcePath(self._CAM_CALIB_IMAGES_DIR), filename), frame)
        
        return result
    ##
    
    def calibratePerform(self):
        # Arrays to store object points and image points from all the images:
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane
        
        images = glob.glob(os.path.join(Resource.getResourcePath(self._CAM_CALIB_IMAGES_DIR), "*.jpg"))
        for name in images:
            frame = cv2.imread(name)
            gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            
            # Find the chess board corners:
            result, corners = self._findGridCorners(frame)
            
            # If found add object points and image points (after refining them):
            if result == True:
                objpoints.append(self._createObjectPoints())
        
                #corners = # Some implementations of below method return it.
                cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), self._CRITERIA)
                imgpoints.append(corners)
                
                # Draw and display corners:
                cv2.drawChessboardCorners(frame, (self._cbCols, self._cbRows), corners, result)
                cv2.imshow("frame", frame)
                cv2.waitKey(1000)
        
        # Perform actual calibration and save result:
        frame = cv2.imread(images[0])
        gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        result, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        numpy.savez(Resource.getResourcePath(self._CAM_CALIB_FILE), ret=result, mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)
        
        cv2.destroyAllWindows()
    ##
    
    def render(self, frame):
        # Find corners of grid in frame:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        result, corners = cv2.findChessboardCorners(gray, (self._cbCols, self._cbRows), None)
        
        # If board detected augment reality:
        if result == True:
            # Prepare axis:
            axis = numpy.float32([
                [0,0,0], [0,3,0], [3,3,0], [3,0,0], [0,0,-3], [0,3,-3], [3,3,-3], [3,0,-3]
            ])
            
            # Project 3D points to frame plane:
            cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), self._CRITERIA)
            rvecs, tvecs, _ = cv2.solvePnPRansac(self._createObjectPoints(), corners, self._mtx, self._dist)
            imgpts = cv2.projectPoints(axis, rvecs, tvecs, self._mtx, self._dist)[0]
            
            # Draw virtual element on real frame:
            self._draw(frame, imgpts)
    ##
  
    def _draw(self, frame, imgpts):
        # Cast coordinates to integer:
        imgpts = numpy.int32(imgpts).reshape(-1,2)
        
        # Predefine colors:
        colorDark = (60,120,40)
        colorLight = (60, 220, 40)
        
        # Draw floorL
        cv2.drawContours(frame, [imgpts[:4]], -1, colorDark, -3)
        
        # Draw columns:
        for i, j in zip(*[range(4), range(4,8)]):
            cv2.line(frame, tuple(imgpts[i]), tuple(imgpts[j]), colorLight, 3)
        
        # Draw ceiling:
        cv2.drawContours(frame, [imgpts[4:]], -1, colorDark, 3)
    ##
    
    def _findGridCorners(self, frame):
        # Find corners of grid in frame:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        result, corners = cv2.findChessboardCorners(gray, (self._cbCols, self._cbRows), None)
        return (result, corners)
    ##
    
    def _createObjectPoints(self):
        # Prepare object points:
        # Like: (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = numpy.zeros((self._cbRows * self._cbCols, 3), numpy.float32)
        objp[:, :2] = numpy.mgrid[0:self._cbCols, 0:self._cbRows].T.reshape(-1, 2)
        return objp
##
