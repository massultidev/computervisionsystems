#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import cv2
import threading

class Camera(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        
        self._event = threading.Event()
        
        self._vidCapture = cv2.VideoCapture(0)
        self._currFrame = self._vidCapture.read()[1]
    ##
    
    def getFrame(self):
        return self._currFrame
    ##
    
    def stop(self):
        self._event.set()
    ##
    
    def run(self):
        try:
            while not self._event.isSet():
                self._currFrame = self._vidCapture.read()[1]
            
            self._event.clear()
            
        except KeyboardInterrupt:
            raise
##
