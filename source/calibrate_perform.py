#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

from computervision.ChessAR import ChessAR

def main():
    try:
        ChessAR().calibratePerform()
        
    except KeyboardInterrupt:
        pass
##

if __name__=="__main__":
    main()
##
