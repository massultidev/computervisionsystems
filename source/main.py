#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import cv2

from computervision.Camera import Camera
from computervision.ChessAR import ChessAR

APP_NAME = "chessAR"
APP_VER = "1.0"

def main():
    camera = Camera()
    chessAR = ChessAR()
    chessAR.loadCalibration()
    
    camera.start()
    
    try:
        while True:
            # Get frame:
            frame = camera.getFrame()
            
            # Render frame:
            chessAR.render(frame)
            
            # Show frame:
            cv2.imshow("Camera", frame)
            cv2.waitKey(20)
        
    except KeyboardInterrupt:
        pass
    
    camera.stop()
##

if __name__=="__main__":
    main()
##
