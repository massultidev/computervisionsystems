#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import cv2

from computervision.Camera import Camera
from computervision.ChessAR import ChessAR

def main():
    imgCount = 0
    imgCountMax = 10
    
    camera = Camera()
    chessAR = ChessAR()
    
    camera.start()
    
    try:
        while imgCount <= imgCountMax:
            # Get frame:
            frame = camera.getFrame()
            
            # Show frame:
            cv2.imshow("Frame", frame)
            cv2.waitKey(3000)
            
            # Collect frames:
            if chessAR.calibrateCollect(frame) == True:
                imgCount += 1
                print("Collected images: ", imgCount)
        
    except KeyboardInterrupt:
        pass
    
    camera.stop()
##

if __name__=="__main__":
    main()
##
